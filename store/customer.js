export const state = () => ({
  devices: []
})

export const mutations = {

  SET_CUSTOMER_DEVICES(state, payload) {
    state.devices = payload
  }

}

export const actions = {
  
  async getDevices({ commit }, payload) {
    await this.$axios.get(`/devices.php?id=${payload}`)
      .then((res) => {
        commit('SET_CUSTOMER_DEVICES', res.data.response.deviceDTOList)
      })
      .catch((err) => {
        console.log('error en store', err)
      })
  },
  deleteDevice({ commit }, payload) {
    commit('SET_CUSTOMER_DEVICES', payload)
  }

}

export const getters = {
  devices: (state) => {
    return state.devices
  }
}
