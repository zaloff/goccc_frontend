export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'universal',
  ssr: false,
  env: {
    /* baseURL: 'http://php' */
    /* baseURL: 'http://172.18.0.3' */
    /* baseURL: 'http://localhost' */
    /* baseURL: 'http://191.103.96.14:3080' */
    /* baseURL: 'http://cccgo.viaccc.com:3080' */
    /* baseURL: 'https://cccgo.viaccc.com:3043' */
    baseURL: 'https://cccgobck.viaccc.com'
  },
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    /* titleTemplate: '%s - ' + process.env.npm_package_name, */
    title: `CCCGO`,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/main.css'
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    '@/plugins/axios',
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth'
  ],
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/login.php', method: 'post', propertyName: 'token' },
          logout: { url: '/logout.php', method: 'post' },
          user: { url: '/login.php', method: 'get', propertyName: 'user' }
        },
        tokenName: 'auth-token',
        tokenType: ''
      }
    },
    redirect: {
      login: '/',
      logout: '/',
      home: '/',
      callback: '/'
    }
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    /* baseURL: 'http://php' */
    /* baseURL: 'http://172.18.0.3' */
    /* baseURL: 'http://localhost' */
    /* baseURL: 'http://191.103.96.14:3080' */
    /* baseURL: 'http://cccgo.viaccc.com:3080' */
    /* baseURL: 'https://cccgo.viaccc.com:3043' */
    baseURL: 'https://cccgobck.viaccc.com'
  },


  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    icons: {
      iconfont: 'mdi'
    },
    theme: {
      options: {
        customProperties: true
      },
      themes: {
        light: {
          primary: '#3b63c3',
          'dark-primary': '#29488b',
          dark: '#1D305B',
          secondary: '#50c1ec',
          success: '#50D94C',
          error: '#ff4757',
          'dark-error': '#930409',
          warning: '#ffa502',
          accent: '#5352ed',
          info: '#a4b0be',
          'light-grey': '#545454',
          'middle-grey': '#3a3a3a',
          'dark-grey': '#222222',
          'secondary-light': '#c9f4ff'
        }
      }
    }
  },
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
  }
}
